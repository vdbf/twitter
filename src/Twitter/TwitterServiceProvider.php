<?php namespace Twitter;

use Illuminate\Support\ServiceProvider;
use Twitter;

class TwitterServiceProvider extends ServiceProvider {

	public function boot()
	{
		$this->package('vdbf/twitter', null, __DIR__.'/../');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->registerTwitter();
	}

	/**
	 * Register the profiler.
	 *
	 * @return void
	 */
	public function registerTwitter()
	{	
		$this->app['twitter'] = $this->app->share(function($app)
		{
			$consumerKey = $app['config']->get('twitter.consumerKey', null);
			$consumerSecret = $app['config']->get('twitter.consumerSecret', null);
			$accessToken = $app['config']->get('twitter.accessToken', null);
			$accessTokenSecret = $app['config']->get('twitter.accessTokenSecret', null);
			return new Twitter($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
		});
	}
}
